#!/bin/bash

SVC_ACCT_FILE="/tmp/eks-admin-service-account.yaml"
CLST_BND_FILE="/tmp/eks-admin-cluster-role-binding.yaml"

cat > $SVC_ACCT_FILE <<End-of-file-1
apiVersion: v1
kind: ServiceAccount
metadata:
  name: eks-admin
  namespace: kube-system
End-of-file-1

cat > $CLST_BND_FILE <<End-of-file-2
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: eks-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: eks-admin
  namespace: kube-system
End-of-file-2

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name>"
   exit 1
else
   CLUSTER="$1"
fi

CLUSTER_FULL="`kubectl config get-contexts | grep "$CLUSTER" | awk '{if ($1 != "*") {print $2} else {print $3}}'`"

# Apply the service account to your cluster
kubectl --cluster="$CLUSTER_FULL" apply -f $SVC_ACCT_FILE 2>&1 > /dev/null
rm -f $SVC_ACCT_FILE

# Apply the cluster role binding to your cluster
kubectl --cluster="$CLUSTER_FULL" apply -f $CLST_BND_FILE 2>&1 > /dev/null
rm -f $CLST_BND_FILE

# Retrieve the token for the service account
echo "SERVICE ACCOUNT TOKEN ="
kubectl --cluster="$CLUSTER_FULL" -n kube-system describe secret $(kubectl --cluster="$CLUSTER_FULL" -n kube-system get secret | grep eks-admin | awk '{print $1}') | grep ^token | awk -F"[ \t]+" '{print $2}'
