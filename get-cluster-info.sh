#!/bin/bash

if [ $# -lt 1 ]; then
   echo "Error: cluster name required."
   echo "Usage: $(basename $0) <cluster name> [region]"
   exit 1
else
   CLUSTER="$1"
   if [ -n "$2" ]; then
      REGION="$2"
   else
      REGION="`aws configure list | awk '/region/{print $2}'`"
   fi
fi

echo "---------------------------------"
./get-url.sh $CLUSTER $REGION
echo "---------------------------------"
./get-cert.sh
echo "---------------------------------"
./get-token.sh $CLUSTER
echo "---------------------------------"
./get-dns.sh $CLUSTER
