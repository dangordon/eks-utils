#!/bin/bash

# WARNING: This script does not yet delete all AWS components created for a cluster.
# There is still work to do and script to add.

if [ "$#" -lt 1 ]; then
   echo "Usage: `basename $0` <cluster name>"
   exit 1
else
   CLUSTER="`kubectl config get-contexts | grep "$1" | awk '{if ($1 != "*") {print $2} else {print $3}}'`"
fi

SERVICE_NAME="`kubectl --cluster=\"$CLUSTER\" get service --all-namespaces | awk '{if ($3 == \"LoadBalancer\") { print $2 }}'`"
SERVICE_NAMESPACE="`kubectl --cluster=\"$CLUSTER\" get service --all-namespaces | awk '{if ($3 == \"LoadBalancer\") { print $1 }}'`"

echo "Deleting LoadBalancer: $SERVICE_NAME"
kubectl --cluster="$CLUSTER" --namespace="$SERVICE_NAMESPACE" delete service "$SERVICE_NAME"

