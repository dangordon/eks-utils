## Amazon AWS Elastic Kubernetes Service (EKS) Utilities

This set of simple shell scripts is meant to capture the command lines that yield the necessary information to set up an Amzon EKS cluster to be used with GitLab.

The scripts assume that you have already configured your system you are running them from to use the AWS and kubectl CLI's. You can find more information on how to [setup your kubectl CLI](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html) and [how to setup your AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) from the Amazon documentation pages.

To use these scripts you must first be logged into the account for the cluster you wish to manage. Use `aws config` to do this.

Todo:
- Add a delete cluster script based off of the information at [https://docs.aws.amazon.com/eks/latest/userguide/delete-cluster.html](https://docs.aws.amazon.com/eks/latest/userguide/delete-cluster.html)
